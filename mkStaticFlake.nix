{ pkgs, inputFlake }:
let
  lockIn = pkgs.lib.importJSON "${inputFlake}/flake.lock";
  isLocalGitRepo = node: ( (node.original.type == "git") && (pkgs.lib.strings.hasPrefix "file:" (node.original.url or "")));
  lockedNode = node: node //
    pkgs.lib.attrsets.optionalAttrs ( (pkgs.lib.hasAttr "locked" node) && !(isLocalGitRepo node) ) {
    locked = {
      type = "path";
      path = "/iso/registry/${builtins.hashString "sha256" node.locked.narHash}";
      narHash = node.locked.narHash;
    };
  };
  lockOut = lockIn // {
    nodes = (pkgs.lib.mapAttrs (_node_name: node: lockedNode node) lockIn.nodes);
  };
  lockOutJson = pkgs.writeText "flake.lock" (builtins.toJSON lockOut);
in
pkgs.runCommand "fixed-flake" { }
  ''
    mkdir $out
    cp -r ${inputFlake}/* $out
    cp --force ${lockOutJson} $out/flake.lock
  ''
